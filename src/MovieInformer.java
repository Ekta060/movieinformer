public class MovieInformer {
    final private String title;
    final private String studio;
    final private String rating;

    MovieInformer(String movieName, String studio, String rating){
        this.title=movieName;
        this.studio=studio;
        this.rating=rating;
    }
    MovieInformer(String movieName, String studio){
        this.title=movieName;
        this.studio=studio;
        this.rating="PG";
    }

    String[] getRatingPG(MovieInformer[] arrayOfMovies){
        String[] ratingIsPG=new String[3];
        int j=0;
        for (MovieInformer Movie : arrayOfMovies) {
            if (Movie.rating.equals("PG")) {
                ratingIsPG[j] = Movie.title;
                j += 1;
            }
        }
        return ratingIsPG;
    }
    public static void main(String[] args){
        MovieInformer movie1= new MovieInformer("Harry Potter","Warner Bros. Picture") ;
        MovieInformer  movie2= new MovieInformer("3-Idiots","VC Films","G") ;
        MovieInformer movie3=new MovieInformer("Casino Royale","Eon Production","PG");
        MovieInformer[] arrayOfObjectMovie=new MovieInformer [3];
        arrayOfObjectMovie [0]=movie1;
        arrayOfObjectMovie[1]=movie2;
        arrayOfObjectMovie[2]=movie3;
        String[] ratedPG=movie1.getRatingPG(arrayOfObjectMovie ) ;
        for (String  movieName:ratedPG
        ) {
            System.out.println(movieName);
        }
    }

}
